# Laravel Robot Battles

This project is made as a part of Comvex's Recruitment Test.  
The web application is a competetive game application that allows users to create robots which can be used to attack other player's robots.

## Stage plan

Stage 1: Database planning and creation. COMPLETE.  
Stage 2: Basic website frame and registration. COMPLETE.  
Stage 3: Authorization. Basic: COMPLETE.   
Stage 4: Robot creation and display. COMPLETE.
Stage 5: Robot battles and leaderboards. Basic: COMPLETE.
Stage 6: User interface. Basic: COMPLETE.

## Documentation
Documentation can be found at 
http://docs.laravelrobotbattles.apiary.io/