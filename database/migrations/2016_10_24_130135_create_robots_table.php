<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRobotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robots', function (Blueprint $table) {
            $table->increments('id');
            // Having this as not a foreign key to be able to
            // delete the user and leave the robot.
            $table->integer('owner_id'); 
            $table->string('name');
            $table->string('avatar');
            $table->integer('wins');
            $table->integer('losses');
            $table->integer('weight');
            $table->integer('power');
            $table->integer('speed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robots');
    }
}
