<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attacker')->unsigned()->onDelete('cascade');
            $table->integer('defender')->unsigned()->onDelete('cascade');
            $table->integer('winner');
            $table->foreign('attacker')->references('id')->on('robots');
            $table->foreign('defender')->references('id')->on('robots');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fights');
    }
}
