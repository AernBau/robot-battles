<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; use Auth;
use App\Http\Requests;

class FightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fights = DB::table('fights')->orderBy('created_at', 'desc')->get();
        if(count($fights))
          return response()->json($fights, 200, [], JSON_PRETTY_PRINT);
        else
          return response('Fights not found', 200);
    }
  
    /**
     * Display a listing of the resource for a single robot.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function indexRobot($id)
    {
        $fightsOfRobot = DB::table('fights')->where('attacker', $id)->orderBy('created_at', 'desc')->get();
        if(count($fightsOfRobot))
          return response()->json($fightsOfRobot, 200, [], JSON_PRETTY_PRINT);
        else
          return response('Robot has not fought', 200);
    }  
    
    /**
     * Display several resources with the highest wins.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function indexRecent()
    {
        $top_winners = DB::table('fights')->orderBy('created_at', 'desc')->take(10)->get();
        if(count($top_winners))
          return response()->json($top_winners, 200, [], JSON_PRETTY_PRINT);
        else
          return response('Robot has not fought', 200);
    } 
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!isset($request['attacker_id']) || !isset($request['defender_id']))
          return response("Missing attacker or defender.", 400);
        
        $one_day_ago = strtotime('-1 day', strtotime(date('Y-m-d H:i:s')));
        $one_day_ago = date("Y-m-d H:i:s A T", $one_day_ago);  
      
        $opts = array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
        $context = stream_context_create($opts);
        $robots = file_get_contents("http://".$_SERVER['HTTP_HOST']."/data/usrbt/".$request['attacker_id'], false, $context);
        $robots = json_decode($robots);
        $previous_fights = 0;
        // Checking how many fights the attacker has participated in in the last 24 hours.
        if (json_last_error() == JSON_ERROR_NONE) {
            $previous_fights = DB::table('fights')
                    ->where('attacker', $request['attacker_id'])
                    ->where('created_at','>',$one_day_ago)
                    ->count();
        }
      
        if($previous_fights >= 5)
          return response("Player's robot has used up all attacks today.", 200);
      
        // If defending robot has been attacked by the attacking robot within 24 hours
        // the action is denied, but understood.
        $fights = DB::table('fights')
                    ->where('attacker', $request['attacker_id'])
                    ->where('defender', $request['defender_id'])
                    ->where('created_at','>',$one_day_ago)
                    ->count();
        
        if($fights)
          return response("Enemy robot already attacked by this robot today.", 200);
      
        $attacker = json_decode(
                      file_get_contents(
                        "http://".$_SERVER['HTTP_HOST']."/data/robots/".$request['attacker_id']
                      )
                    );
        
        $defender = json_decode(
                      file_get_contents(
                        "http://".$_SERVER['HTTP_HOST']."/data/robots/".$request['defender_id']
                      )
                    );
      
        // THE BATTLE ALGORITHM
      
        // If Victory is 1, the attacker wins. Weight < Power < Speed < Weight
        // Like rock, paper, scissors. Highest attribute = primary.
        // Battle Power = primary attribute 
        //            + (if attribute bonus against) + secondary + tertiary
        $primary_atk = 0;
        $a_stats = [$attacker->weight, $attacker->power, $attacker->speed];
        $primary_num = max($a_stats);
        if($primary_num == $attacker->weight){ $primary_atk = 0; }
        else if($primary_num == $attacker->power){ $primary_atk = 1; }
        else if($primary_num == $attacker->speed){ $primary_atk = 2; }
         
        $primary_def = 0;
        $d_stats = [$defender->weight, $defender->power, $defender->speed];
        $primary_num = max($d_stats);
        if($primary_num == $defender->weight){ $primary_def = 0; }
        else if($primary_num == $defender->power){ $primary_def = 1; }
        else if($primary_num == $defender->speed){ $primary_def = 2; }
      
        $victory = 1;
        
        if($primary_atk == $primary_def){
          if($a_stats[$primary_atk] > $d_stats[$primary_def])
            $victory = 0;
        } else {
          // Compressing these is possible, but reduces readability heavily.
          // WEIGHT VS POWER
          if($primary_atk == 0 && $primary_def == 1){
            $atk_pwr = $a_stats[0] + $a_stats[1]*0.3 + $a_stats[2]*0.1;
            $def_pwr = $d_stats[1] + $d_stats[1]*0.1+ $d_stats[2]*0.3 + $d_stats[0]*0.1;
          } else if ($primary_atk == 1 && $primary_def == 0){
            $atk_pwr = $a_stats[1] + $a_stats[1]*0.1 + $a_stats[2]*0.3 + $a_stats[0]*0.1;
            $def_pwr = $d_stats[0] + $d_stats[1]*0.3 + $d_stats[2]*0.1;
          }
          
          // POWER VS SPEED
          if($primary_atk == 1 && $primary_def == 2){
            $atk_pwr = $a_stats[1] + $a_stats[2]*0.3 + $a_stats[0]*0.1;
            $def_pwr = $d_stats[2] + $d_stats[2]*0.1+ $d_stats[0]*0.3 + $d_stats[1]*0.1;
          } else if ($primary_atk == 2 && $primary_def == 1){
            $atk_pwr = $a_stats[2] + $a_stats[2]*0.1 + $a_stats[0]*0.3 + $a_stats[1]*0.1;
            $def_pwr = $d_stats[1] + $d_stats[2]*0.3 + $d_stats[0]*0.1;
          }
          
          // SPEED VS WEIGHT
          if($primary_atk == 2 && $primary_def == 0){
            $atk_pwr = $a_stats[2] + $a_stats[0]*0.3 + $a_stats[1]*0.1;
            $def_pwr = $d_stats[0] + $d_stats[0]*0.1+ $d_stats[1]*0.3 + $d_stats[2]*0.1;
          } else if ($primary_atk == 0 && $primary_def == 2){
            $atk_pwr = $a_stats[0] + $a_stats[0]*0.1 + $a_stats[1]*0.3 + $a_stats[2]*0.1;
            $def_pwr = $d_stats[2] + $d_stats[0]*0.3 + $d_stats[1]*0.1;
          }
          
          if($def_pwr > $atk_pwr){ $victory = 0; }
        }
      
        // Post-battle database actions
        DB::table('fights')->insert(
          [
            'attacker' => $attacker->id,
            'defender' => $defender->id,
            'winner' => $victory,
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
          ]
        );
      
        if($victory){
          DB::table('robots')
            ->where('id', $attacker->id)
            ->update(['wins' => $attacker->wins+1]);
          DB::table('robots')
            ->where('id', $defender->id)
            ->update(['losses' => $defender->losses+1]);
          return response("Fight recorded, winner is ATTACKER: ".$attacker->name, 201);
        } else {
          DB::table('robots')
            ->where('id', $defender->id)
            ->update(['wins' => $defender->wins+1]);
          DB::table('robots')
            ->where('id', $attacker->id)
            ->update(['losses' => $attacker->losses+1]);
          return response("Fight recorded, winner is DEFENSE: ".$defender->name, 201);
        }
      
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $fight = DB::table('fights')->where('id', $id)->get();
      if(count($fight))
          return response()->json($fight, 200, [], JSON_PRETTY_PRINT);
      else
          return response('Fight not found', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      return response('501 Not Implemented', 501);
    }
}
