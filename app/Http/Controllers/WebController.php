<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class WebController extends Controller
{
    public function home(){
      return view('home');
    }
    
    public function register(){
      return view('pages.register');
    }
  
    public function login(){
      return view('pages.login');
    }
  
    public function profile($id){
      $owner = false;
      if(Auth::check())
        if(Auth::user()->id == $id)
          $owner = true;
      
      $data = [];
      
      $opts = array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
      $context = stream_context_create($opts);
      $robots = file_get_contents(
        "http://".$_SERVER['HTTP_HOST']."/data/usrbt/".Auth::user()->id, false, $context);
      $robots = json_decode($robots);
      $data['robots'] = $robots;

      $data['owner'] = $owner;
      if(!$owner){
        $user = json_decode(file_get_contents(
          "http://".$_SERVER['HTTP_HOST']."/data/users/".$id));
        if (json_last_error() == JSON_ERROR_NONE) {
          $data['user'] = $user;
        }
      } 
      
      return view('pages.profile', $data);
    }
    
    public function battle(){
      // Retrieving user's robots and checking how many fights they have done today.
      $opts = array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
      $context = stream_context_create($opts);
      $robots = file_get_contents(
        "http://".$_SERVER['HTTP_HOST']."/data/usrbt/".Auth::user()->id, false, $context);
      $robots = json_decode($robots);
      if (json_last_error() == JSON_ERROR_NONE) {
        foreach($robots as $robot){
            $previous_fights = file_get_contents(
              "http://".$_SERVER['HTTP_HOST']."/data/fights/attack/".$robot->id);
            $fights_in_past_day = 0;
            json_decode($previous_fights);
            if (json_last_error() == JSON_ERROR_NONE) {
              foreach(json_decode($previous_fights) as $fight){
                if(strtotime($fight->created_at) > strtotime('-1 day', strtotime(date('Y-m-d H:i:s')))){
                  $fights_in_past_day++;
                } 
              }
              $robot->fights_today = $fights_in_past_day;
            }
        }
        $data['player_robots'] = $robots;
      }
      // If less than or 5 enemy robots exist, all are shown.
      // If there are more than 5 enemy robots, 5 are randomly selected.
      // An improvement would be filtering win percentages to be closer to yours.
      $enemy_robots = json_decode(file_get_contents(
        "http://".$_SERVER['HTTP_HOST']."/data/enrbt/".Auth::user()->id, false, $context));
      $amount_of_random = 5;
      if(count($enemy_robots) < 5){
        $amount_of_random = count($enemy_robots);
      }
      if($amount_of_random == 0){
        return view('pages.battle', $data);
      }
      $rand_keys = array_rand($enemy_robots, $amount_of_random);
      $enemy_ext = []; 
      if($amount_of_random > 1){
        foreach($rand_keys as $key){
          $player = json_decode(file_get_contents(
            "http://".$_SERVER['HTTP_HOST']."/data/users/".$enemy_robots[$key]->owner_id));
          $enemy_robots[$key]->username = $player->username;
          $enemy_ext[] = $enemy_robots[$key];
        }
      } else if($amount_of_random == 1) {
        $player = json_decode(file_get_contents(
          "http://".$_SERVER['HTTP_HOST']."/data/users/".$enemy_robots[$rand_keys]->owner_id));
        $enemy_robots[0]->username = $player->username;
        $enemy_ext[] = $enemy_robots[0];
      }
      $data['enemy_robots'] = $enemy_ext;
      
      return view('pages.battle', $data);
    }
  
    public function fights(){
      // IN PROGRESS
      $data['recent_10'] = json_decode(file_get_contents(
        "http://".$_SERVER['HTTP_HOST']."/data/fights/recent"));
      $data['top_robots'] = json_decode(file_get_contents(
        "http://".$_SERVER['HTTP_HOST']."/data/robots/top"));
      return view('pages.fights', $data);
    }

    public function robots(){
      $opts = array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
      $context = stream_context_create($opts);
      $robots = file_get_contents(
        "http://".$_SERVER['HTTP_HOST']."/data/usrbt/".Auth::user()->id, false, $context);
      $data = [];
      json_decode($robots);
      if (json_last_error() == JSON_ERROR_NONE) 
        $data['robots'] = json_decode($robots);
      return view('pages.robots', $data);
    }
}
