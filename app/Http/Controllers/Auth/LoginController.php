<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
  
    public function postLogin(Request $request)
    { 
      $credentials = array(
          'username' => $request['username'],
          'password' => $request['password']
      );
      if(Auth::attempt($credentials)) {
          return response()->view('home', [], 200);
      } else {
          $error = "User credentials incorrect.";
          return response()
                  ->view('pages.login', ['errors' => $error], 400);
      }
    }
  
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
