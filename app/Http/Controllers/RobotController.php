<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Auth;

class RobotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $robots = DB::table('robots')->get();
        if(count($robots))
          return response()->json($robots, 200, [], JSON_PRETTY_PRINT);
        else
          return response('There are no robots', 200);
    }

    /**
     * Display a listing of the resource of the logged in user.
     *
     * @return \Illuminate\Http\Response
     */
    public function userIndexById($id)
    {
        $robots = DB::table('robots')
            ->where('owner_id', $id)
            ->orderBy('wins', 'desc')->get();
        if(count($robots))
          return response()->json($robots, 200, [], JSON_PRETTY_PRINT);
        else
          return response('User has no robots', 200);
    }
  
    /**
     * Display a listing of the resource excluding logged in user.
     *
     * @return \Illuminate\Http\Response
     */
    public function exclIndex($id)
    {
        $robots = DB::table('robots')
            ->where('owner_id', '!=', $id)->get();
        if(count($robots))
          return response()->json($robots, 200, [], JSON_PRETTY_PRINT);
        else
          return response('There are no enemy robots', 200);
    }

    /**
     * Display several most recently created resources.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function indexTop()
    {
        $fightsOfRobot = DB::table('robots')
            ->orderBy('wins', 'desc')->take(10)->get();
        if(count($fightsOfRobot))
          return response()->json($fightsOfRobot, 200, [], JSON_PRETTY_PRINT);
        else
          return response('Robot has not fought', 200);
    } 
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // All robots get 100 points to allot.
      // The numbers put into the Weight, Power and Speed fields
      // will be adjusted proportionately to fit into the 100.
      
      if(isset($request['file'])){
        $file = $request['file']->openFile();
        $data_from_file = str_getcsv($file);
        $name = $data_from_file[0];
        $avatar = $data_from_file[1];
        $weight = $data_from_file[2];
        $power = $data_from_file[3];
        $speed = $data_from_file[4];
      } else {
        $name = $request['name'];
        $avatar = $request['avatar'];
        $weight = $request['weight'];
        $power = $request['power'];
        $speed = $request['speed'];
      }
      
      $total = $weight + $power + $speed;
      $adj_weight = 100 * $weight / $total;
      $adj_power = 100 * $power / $total;
      $adj_speed = 100 * $speed / $total;
      DB::table('robots')->insert(
        [
          'name' => $name,
          'avatar' => $avatar,
          'owner_id' => Auth::user()->id,
          'wins' => '0',
          'losses' => '0',
          'weight' => $adj_weight,
          'power' => $adj_power,
          'speed' => $adj_speed,
          'created_at' => DB::raw('NOW()'),
          'updated_at' => DB::raw('NOW()'),
        ]
      );
      
      return response('Robot created', 201);
    }
  
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $robot = DB::table('robots')->where('id', $id)->first();
      if(count($robot))
        return response()->json($robot, 200, [], JSON_PRETTY_PRINT);
      else
        return response('Resource not found', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      return response('501 Not Implemented', 501);
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateReq(Request $request)
    {
      if($request->id == null){
        return response('No resource selected', 400);
      }

      $old = DB::table('robots')->where('id', $request->id)->get();  
      if($request['name'] == '')
        $request['name'] = $old[0]->name;
      if($request['avatar'] == '')
        $request['avatar'] = $old[0]->avatar;

      // Wins and losses reset if there are changes to the stats.
      $changed = false;
      if($request['weight'] == ''){
        $request['weight'] = $old[0]->weight;
        $changed = true;
      }
      if($request['power'] == ''){
        $request['power'] = $old[0]->power;
        $changed = true;
      }
      if($request['speed'] == ''){
        $request['speed'] = $old[0]->speed;
        $changed = true;
      }

      $total = $request['weight']+$request['power']+$request['speed'];
      $adj_weight = ceil(100 * $request['weight'] / $total);
      $adj_power = ceil(100 * $request['power'] / $total);
      $adj_speed = ceil(100 * $request['speed'] / $total);

      $stats = $adj_weight + $adj_power + $adj_speed;
      if($stats > 100)
        $adj_power = $adj_power - ($stats - 100);

      if(!$changed){
        DB::table('robots')->where('id', $request->id)
          ->update(
              ['name' => $request['name'], 
               'avatar' => $request['avatar'],
               'weight' => $adj_weight,
               'power' => $adj_power,
               'speed' => $adj_speed,
               'updated_at' => DB::raw('NOW()')]);
      } else {
        DB::table('robots')->where('id', $request->id)
          ->update(
              ['name' => $request['name'], 
               'avatar' => $request['avatar'],
               'wins' => '0',
               'losses' => '0',
               'weight' => $adj_weight,
               'power' => $adj_power,
               'speed' => $adj_speed,
               'updated_at' => DB::raw('NOW()')]);
      }

      return response('Robot updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      return response('501 Not Implemented', 501);
    }
  
    /**
     * Remove a passed resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
      if($request->id == null){
          return response('No resource selected', 400);      
      }
      
      DB::table('robots')->where('id', $request->id)->delete();
      return response('Resource deleted', 200);
    }
}
