<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Hash; use DB;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = DB::table('users')->select('id', 'username')->get();
      if(count($user))
        return response()->json($user, 200, [], JSON_PRETTY_PRINT);
      else
        return response('There are no users', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'username'  => 'required|min:3|unique:users,username|max:30',
        'password'  => 'required|min:4|max:60 ',
      ]);

      $reg_info = $request->all();

      if ($validator->fails())
      {
        return response()
          ->view('pages.register', ['errors' => $validator->errors()], 400);
      }
      else {
        $reg_info['password'] = Hash::make($reg_info['password']);
        DB::table('users')->insert(
          [
            'username' => $reg_info['username'],
            'password' => $reg_info['password'],
            
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
          ]
        );
        return response('User created', 201);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = DB::table('users')->select('id', 'username')->where('id', $id)->first();
      if(count($user))
        return response()->json($user, 200, [], JSON_PRETTY_PRINT);
      else
        return response('User not found', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response('501 Not Implemented', 501);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      return response('501 Not Implemented', 501);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if($id == null){
        return response('No resource selected', 400);      
      }

      DB::table('users')->where('id', $id)->delete();
      return response('Resource deleted', 200);
    }
}
