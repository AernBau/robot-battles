<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'WebController@home');
Route::get('/home', 'WebController@home');
Route::get('/fights', 'WebController@fights');
Route::get('/profile/{id}', 'WebController@profile');

Route::get('/register', 'WebController@register');
Route::get('/login',  'WebController@login');

Route::post('/login', 'Auth\LoginController@postLogin');
Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('data/users', 'UserController', ['exclude' => ['update', 'destroy']]);

$public_methods = ['index', 'show', 'create', 'edit'];
Route::get('/data/robots/top', 'RobotController@indexTop');

Route::resource('data/robots', 'RobotController', ['only' => $public_methods]);

Route::get('/data/fights/attack/{id}', 'FightController@indexRobot');
Route::get('/data/fights/recent', 'FightController@indexRecent');
Route::resource('data/fights', 'FightController', ['only' => $public_methods]);


Route::group(['middleware' => 'auth'], function(){  
  Route::get('/robots', 'WebController@robots');
  Route::get('/battle', 'WebController@battle');
    
  Route::resource('data/users', 'UserController', ['only' => ['update', 'destroy']]);
  
  $private_methods = ['store', 'update', 'destroy'];
  
  Route::get('/data/usrbt/{id}', 'RobotController@userIndexById');
  Route::get('/data/enrbt/{id}', 'RobotController@exclIndex');
  Route::put('/data/robots/update', 'RobotController@updateReq');
  Route::delete('/data/robots/', 'RobotController@delete');
  Route::resource('/data/robots', 'RobotController', ['only' => $private_methods]);
  Route::resource('/data/fights', 'FightController', ['only' => $private_methods]);
});