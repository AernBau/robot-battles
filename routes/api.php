<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
*/




//get fights needs to be public, no api permission
//data from those fights needs to be public, but nothing else
//post/update/delete must use middleware
//   