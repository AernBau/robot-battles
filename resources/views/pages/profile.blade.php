@extends('layouts.default')

@section('content')
<style>
  #profile{
    padding: 20px;
  }
  
  .robot {
      display: inline-block;
      padding: 10px;
  }
  
  .robot div div {
      display: inline-flex;
      padding: 5px;
  }
  
  #delete {
    position: absolute;
    right: 0;
    bottom: 100px;
  }
  
</style>
<div id="profile">
  
  <div id="user">
    <?php
      if($owner){
        echo "<h1>".Auth::user()->username."</h1>";
      } else {
        if(isset($user)){
          echo "<h1>".$user->username."</h1>";
        } else {
          echo "<h1>User not found</h1>";
        }
      }
      if($owner || isset($user)){
        $wins = 0; $losses = 0;
        if(isset($robots)){
          foreach($robots as $robot){
            $wins += $robot->wins; $losses += $robot->losses;
          }
          $total = $wins+$losses;
          echo "<h2>Battles: ".$total." | Wins: ".$wins." | Losses: ".$losses."</h2>";
        } else {
          echo "<h2>User has no robots</h2>";
        }
      }
    ?>
  </div>
  <div id="robots">
    <?php
    if($owner || isset($user)){
      if(!isset($robots)){
        echo "<h1>User has no robots</h1>";
      } else {
        foreach($robots as $robot){
          echo "<div class='robot'>";
            echo "<div class='title'>";
              echo "<div id='name'>".$robot->name."</div>";
              echo "<img src='avatar' alt='".$robot->avatar."'></img>";
            echo "</div><div class='score'>";
              echo "<div id='wins'>Wins: ".$robot->wins."</div>";
              echo "<div id='losses'>Losses: ".$robot->losses."</div>";
            echo "</div><div class='stats'>";
              echo "<div id='weight'>Wgt: ".$robot->weight."</div>";
              echo "<div id='power'>Pwr: ".$robot->power."</div>";
              echo "<div id='speed'>Spd: ".$robot->speed."</div>";
          echo "</div></div>";
        }  
      }
    }
    ?>
  </div>
  <?php
    if($owner){
      echo "<div id='delete'>";
      echo "<form method=\"POST\" action=\"http://laravelrobots.dev/data/users/".Auth::user()->id."\" accept-charset=\"UTF-8\">";
      echo "<input name=\"_method\" type=\"hidden\" value=\"delete\"/>";
      echo "<input type=\"submit\" value=\"DELETE USER\">";
      echo "</form></div>";
    }
  ?>
</div>
@endsection