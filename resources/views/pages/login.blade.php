@extends('layouts.default')

@section('content')
  <!-- Create a form, absolute position, width=x, margin-right=x/2, top: 50%, left: 50% -->
  <style>
    #center_form {
      margin: auto;
      padding-top: 20%;
      transform: translateY(-2%);
      width: 400px;
    }
    
    form input {
      width: 100%;
      height: 40px;
      background-color: white;
      border-style: groove;
      padding: 0px;
      font-family: "Open Sans";
      font-size: 16px;
      padding: 5px 10px;
      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
      -moz-box-sizing: border-box;    /* Firefox, other Gecko */
      box-sizing: border-box;         /* Opera/IE 8+ */
    }
        
  </style>
  <div id="center_form">
    {!! Form::open(['url' => '/login']) !!}
      {!! Form::text('username','',['required', 'placeholder' => 'Username']) !!}
      <br>
      {!! Form::text('password','',['required', 'placeholder' => 'Password']) !!}
      <br>
      {!! Form::submit('Login') !!}
    {!! Form::close() !!}
  </div>
</div>
  <div id="errors" ><?php 
    if(isset($errors))
      if(is_string($errors)){
        echo "<div class='error'>";
        echo $errors;
        echo "</div>";
      } else {
        foreach ($errors->all() as $message) {
          echo "<div class='error'>";
          echo $message;
          echo "</div>";
        }
      }
    
  ?>
@endsection