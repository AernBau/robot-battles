@extends('layouts.default')

@section('content')
  <!-- Create a form, absolute position, width=x, margin-right=x/2, top: 50%, left: 50% -->
  <style>
    #center_form {
      margin: auto;
      padding-top: 20%;
      transform: translateY(-2%);

      width: 400px;
    }
    
    form label, input{
      height: 40px;
    }
    
    form label {
      width: 125px;
      display: inline-block;
    }
    
    form input {
      width: 265px;
      background-color: white;
      border-style: groove;
      padding: 0px;
      font-family: "Open Sans";
      font-size: 16px;
      padding: 5px 10px;
      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
      -moz-box-sizing: border-box;    /* Firefox, other Gecko */
      box-sizing: border-box;         /* Opera/IE 8+ */
    }
        
  </style>
  <div id="center_form">
  {!! Form::open(['url' => '/data/users/']) !!}
    {!! Form::label('username','Username') !!} 
    {!! Form::text('username','',['required', 'placeholder' => '3 - 30 characters']) !!}
    <br>
    {!! Form::label('password','Password') !!}
    {!! Form::text('password','',['required', 'placeholder' => '4 - 60 characters']) !!}
    <br>
    {!! Form::label('') !!}
    {!! Form::submit('Register') !!}
  {!! Form::close() !!}
  </div>
</div>
  <div id="errors" ><?php 
    foreach ($errors->all() as $message) {
      echo "<div class='error'>";
      echo $message;
      echo "</div>";
    }
  ?>
@endsection