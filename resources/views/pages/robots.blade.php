@extends('layouts.default')

@section('content')
<style>
  .form {
      padding: 20px;
      width: 400px;
      display: table-cell;
    
      border-width: 1px;
      border-style: double;
      border-left: none;
      border-top: none;
      border-bottom: none;
  }
  
  form label, input, select{
      height: 35px;
  }

  form label {
      width: 120px;
      display: inline-block;
  }
  
  form input, form select {
      width: calc(100% - 125px);
      background-color: white;
      border-style: groove;
      padding: 0px;
      font-family: "Open Sans";
      font-size: 16px;
      padding: 5px 10px;
      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
      -moz-box-sizing: border-box;    /* Firefox, other Gecko */
      box-sizing: border-box;         /* Opera/IE 8+ */
  }
  
  #your_robots {
      padding: 20px;
      height: 220px;
  }
  
  .robot {
      display: inline-block;
      padding: 10px;
  }
  
  .robot div {
      
  }
  
  .robot div div {
      display: inline-flex;
      padding: 5px;
  }
  
  .title, .score, .stats {
      
  }
  
  #update, #create, #delete {
    width: 100%;
  }
  
  #delete {
    border-style: double;
    border-width: 1px;
    border-top: none;
    border-right: none;
    border-left: none;
    padding-bottom: 40px;
  }
  
</style>

<div id="your_robots">
  <h2>Your robots:</h2>
  <?php 
    
    if(!isset($robots)){
      echo "<h1>You have no robots, make one below</h1>";
    } else {
      foreach($robots as $robot){
        echo "<div class='robot'>";
          echo "<div class='title'>";
            echo "<div id='name'>".$robot->name."</div>";
            echo "<img src='avatar' alt='".$robot->avatar."'></img>";
          echo "</div><div class='score'>";
            echo "<div id='wins'>Wins: ".$robot->wins."</div>";
            echo "<div id='losses'>Losses: ".$robot->losses."</div>";
          echo "</div><div class='stats'>";
            echo "<div id='weight'>Wgt: ".$robot->weight."</div>";
            echo "<div id='power'>Pwr: ".$robot->power."</div>";
            echo "<div id='speed'>Spd: ".$robot->speed."</div>";
        echo "</div></div>";
      }  
    }
  ?>
  
</div>
<div id="cud_robots">
  <div class="form">
    <div id="create">
      {!! Form::open(['url' => '/data/robots']) !!}
        {!! Form::label('name','Name') !!} 
        {!! Form::text('name','',['required', 'placeholder' => 'Name of robot']) !!}
        <br>
        {!! Form::label('avatar','Avatar') !!} 
        {!! Form::text('avatar','',['required', 'placeholder' => 'Link would go here']) !!}
        <br>
        {!! Form::label('weight','Weight') !!} 
        {!! Form::text('weight','',['required', 'placeholder' => 'Weight']) !!}
        <br>
        {!! Form::label('power','Power') !!} 
        {!! Form::text('power','',['required', 'placeholder' => 'Power']) !!}
        <br>
        {!! Form::label('speed','Speed') !!} 
        {!! Form::text('speed','',['required', 'placeholder' => 'Speed']) !!}
        <br>
        {!! Form::label('','') !!}
        {!! Form::submit('Build Robot') !!}
      {!! Form::close() !!}
    </div>
  </div>
  <div class="form">
    <div id="update">
      {!! Form::open(['url' => '/data/robots/update']) !!}
        <input name="_method" type="hidden" value="put"/>
        {!! Form::label('id','Robot name') !!}
        <?php 
          $select_data = [];
          if(isset($robots))
            foreach($robots as $robot){
              $select_data[$robot->id] = $robot->name;
            }
        ?>
        {!! Form::select('id',$select_data,['required']) !!}
        {!! Form::label('name','New name') !!} 
        {!! Form::text('name','',['placeholder' => 'New name']) !!}
        <br>
        {!! Form::label('avatar','Avatar') !!} 
        {!! Form::text('avatar','',['placeholder' => 'Only change fields needed']) !!}
        <br>
        {!! Form::label('weight','Weight') !!} 
        {!! Form::text('weight','',['placeholder' => 'Weight']) !!}
        <br>
        {!! Form::label('power','Power') !!} 
        {!! Form::text('power','',['placeholder' => 'Power']) !!}
        <br>
        {!! Form::label('speed','Speed') !!} 
        {!! Form::text('speed','',['placeholder' => 'Speed']) !!}
        <br>
        {!! Form::label('','') !!}
        {!! Form::submit('Update Robot') !!}
      {!! Form::close() !!}
    </div>
  </div>
  <div class="form">
    <div id="delete">
      {!! Form::open(['url' => '/data/robots/', ]) !!}
        <input name="_method" type="hidden" value="delete"/>
        {!! Form::label('id','Robot name') !!}
        <?php 
          $select_data = [];
          if(isset($robots))
            foreach($robots as $robot){
              $select_data[$robot->id] = $robot->name;
            }
        ?>
      {!! Form::select('id',$select_data,['required']) !!}
        {!! Form::label('','') !!}
        {!! Form::submit('Delete Robot') !!}
      {!! Form::close() !!}
    </div>
    <br>
    <div id="import">
      {!! Form::open(['url' => '/data/robots/', 'files' => true]) !!}
        {!! Form::label('file','CSV File') !!}
        {!! Form::file('file', ['accept' => '.csv, text/csv']) !!}
        {!! Form::label('','') !!}
        {!! Form::submit('Import Robot') !!}
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection