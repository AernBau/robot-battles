@extends('layouts.default')

@section('content')
<style>
  .robot {
      display: block;
      padding: 10px;
      height: 100px;
  }
  
  .robot div {
      
  }
  
  .robot div div {
      display: inline-flex;
      padding: 5px;
  }
  
  .attacker, .defender {
      position: absolute;
      padding: 20px;
  }
  
  .defender {
      right: 0;
  }
  
  .battle {
    position: absolute;
    left: 50%;
    top: 40%;
  }
    
</style>
<div id="battle_arena">
  {!! Form::open(['url' => '/data/fights']) !!}
  <div class="attacker">
    <?php 
    $player_select = [];
    $enemy_select = [];
    if(!isset($player_robots)){
      echo "<h1 style='width: 200px;'>You have no robots, make one below</h1>";
    } else {
      foreach($player_robots as $pl_robot){
        echo "<div class='robot'>";
          echo "<div class='title'>";
            echo "<div id='name'>".$pl_robot->name."</div>";
            echo "<img src='avatar' alt='".$pl_robot->avatar."'></img>";
          echo "</div><div class='score'>";
            echo "<div id='wins'>Wins: ".$pl_robot->wins."</div>";
            echo "<div id='losses'>Losses: ".$pl_robot->losses."</div>";
            if(isset($pl_robot->fights_today)){
              echo "<div id='daily'>Fights today: ".$pl_robot->fights_today."</div>";
            } else {
              echo "<div id='daily'>No fights at all</div>";
            }
          echo "</div><div class='stats'>";
            echo "<div id='weight'>Wgt: ".$pl_robot->weight."</div>";
            echo "<div id='power'>Pwr: ".$pl_robot->power."</div>";
            echo "<div id='speed'>Spd: ".$pl_robot->speed."</div>";
        // Click to select goes here.
        echo "</div></div>";
        if(isset($pl_robot->fights_today))
          if($pl_robot->fights_today >= 5)
            break;
        $player_select[$pl_robot->id] = $pl_robot->name;
      }  
    }
    
    ?>
  </div>
  <div class="battle">
    <?php 
      if(isset($player_robots)){
        echo Form::select('attacker_id', $player_select, ['required']);
      } 
      echo Form::submit('BATTLE');
      if(isset($enemy_robots)){
        foreach($enemy_robots as $en_robot){
            $enemy_select[$en_robot->id] = $en_robot->name;
        }
        echo Form::select('defender_id', $enemy_select, ['required']);
      }
    ?>
  </div>
  <div class="defender">
    <?php 
    
    if(!isset($enemy_robots)){
      echo "<h1 style='width: 200px;'>There are no opposing robots</h1>";
    } else {
      foreach($enemy_robots as $en_robot){
        echo "<div class='robot'>";
          // Click to select goes here.
          echo "<div class='title'>";
            echo "<div id='name'>".$en_robot->username."'s ".$en_robot->name."</div>";
            echo "<img src='avatar' alt='".$en_robot->avatar."'></img>";
          echo "</div><div class='score'>";
            echo "<div id='wins'>Wins: ".$en_robot->wins."</div>";
            echo "<div id='losses'>Losses: ".$en_robot->losses."</div>";
        // The player cannot see his enemy's stats.
        // Should display a hint, like ratio between primary and secondary.
        echo "</div></div>";
      }  
    }
    
    ?>
  </div>
  {!! Form::close() !!}
</div>
@endsection