@extends('layouts.default')

@section('content')
<style>
  .board {
    padding: 20px;
  }
</style>
<div id="fights">
  <div id="top_robots" class="board">
    Top 10 robots
    <br>
    <?php 
      // Need to also retrieve names of attackers and defenders
      $place = 1;
      if(isset($top_robots))
        foreach($top_robots as $robot){
          $fights = $robot->wins + $robot->losses;
          echo "<div id='top".$robot->id."'>Rank ".$place." - ".$robot->name." ";
          echo "| Battles: ".$fights." Wins: ".$robot->wins." Losses: ".$robot->losses;
          echo "</div>";
          $place++;
        }
    ?>
  </div>
  <br>
  <div id="recent" class="board">
    10 most recent fights
    <br>
    <?php 
      // Need to also retrieve names of attackers and defenders
      if(isset($recent_10))
        foreach($recent_10 as $fight){
          echo "<div id='fight".$fight->id."'>Battle ".$fight->id." : Robot ".$fight->attacker." vs Robot ".$fight->defender." ";
          if($fight->winner)
            echo "| Winner: Robot ".$fight->attacker;
          else 
            echo "| Winner: Robot ".$fight->defender;
          echo "</div>";
        }
    ?>
  </div>
</div>
@endsection