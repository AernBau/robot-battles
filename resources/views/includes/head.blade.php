<!DOCTYPE html>
<meta charset="utf-8">

<title>Digital Robot Battle Royale</title>

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

<script src="https://unpkg.com/jquery@3.1.0/dist/jquery.min.js"></script>

<style>
  html {
    min-width: 840px;
    min-height: 600px;
    background-color: #DDD;
    overflow: hidden;
  }
  
  body {
    margin: 0px;
    font-family: "Open Sans";
  }
  
  header, footer{
    background-color: #333;
    width: 100%;
    z-index: 9000;
  }
  
  header {
    position: absolute;
  }
  
  header #logo, footer{
    display: inline-block;
    text-align: center;
    padding: 10px;
    background-color: #999;
    color: #EEE;
  }
  
  header #logo{
    font-size: calc(18px + 1vw);
    padding: 10px 20px;
    text-decoration: none;
  }

  footer {
    position: absolute;
    bottom: 0;
    padding: 0;
    height: 5%;
    min-height: 30px;
  }
  
  #copyright {
    float: right;
    padding: 0.5% 20px;
  }
  
  #errors {
    color: #D44; 
    position: absolute;
    top:  50%;
    right: -15%;
    width:  400px;
    transform: translate(-50%, -50%);
    pointer-events: none;
    z-index: 999;
  }

  #errors .error {
    padding: 10px;
    pointer-events: none;
  }

  #sidebar {
    background-color: #AAA;
    position: absolute;
    top: 0;
    left: 0;
    width: 240px;
    height: 100%;
  }
 
  #greet {
    float: left;
    clear: both;
    line-height: 3;
    font-size: calc(12px + 1.5vw);
    text-align: center;
    width: 100%;
    border-style: groove;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    border-color: #BBB;
    color: #444;
    transition: all 0.3s ease 0s;
  }
  
  #sidebar ul {
    list-style: none;
    padding: 0;
    margin: 0;
    display: table;
    width: 100%;
    color: #CCC;
  }
   
  #sidemenu {
    height: 100%;  
  }
  
  #sidebar ul button {
    padding: 5% 0px;
    text-align: center;
    width: 100%;
    outline: none;
    background-color: #BBB;
    border-left: 0;
    border-right: 0;
    border-style: none;
    transition: all 0.3s ease 0s;
    color: #555;
    font-size: calc(10px + 1vw);
    font-family: "Lato";
  }
  
  #sidebar ul button:nth-child(odd) {
    background-color: #CCC;
  }
  
  #sidebar ul button:hover {
    background-color: #EEE;
    color: #222;
  }
  
  #sidebar ul button:first-child {
    border-top: 0;
  }
  
  #sidebar ul button:last-child {
    border-bottom: 0;
  }
  
  #dashcontent {
    background-color: #DDD;
    position: absolute;
  }
    
</style>