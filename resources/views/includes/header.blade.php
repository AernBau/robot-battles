<!DOCTYPE html>
<header id="sidebar">
  <a id="logo" href="/">Digital Robot Battle Royale</a>
  <div id="sidemenu">
    <ul>
      <br>
      <div style="display: flex;">
      @if(Auth::check())
        <?php
          echo "<button onclick=\"location.href = '/profile/".Auth::user()->id."';\">".Auth::user()->username."'s profile</button>";
        ?>
      
      <button onclick="location.href = '/logout';">Logout</button>
      @else
      
        <button onclick="location.href = '/register';">Register</button>
        <button onclick="location.href = '/login';">Log in</button>
      
      @endif
        </div>
      <br>
      <button onclick="location.href = '/';">Home</button>
      @if(Auth::check())
      <button onclick="location.href = '/robots';">Your robots</button>
      @endif
      <button onclick="location.href = '/battle';">Battle</button>
      <button onclick="location.href = '/fights';">Fights</button>
    </ul>
  </div>
</header>