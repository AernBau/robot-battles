<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  @include('includes.head')
</head>
<body>
  @include('includes.header')
  
  <div class='container' style="width: calc(100% - 240px); padding-left: 240px;">
    
    <div id="main" >
      @yield('content')
    </div>
    
  </div>
  
  @include('includes.footer')
</body>
</html>